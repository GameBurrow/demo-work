# Requirements
- Docker & docker-composer (docker-desktop includes both)

# Starting this project
1. Install Docker & docker-compose, follow your OS specific instructions: https://docs.docker.com/desktop/
2. Copy `php.env.example` to `php.env`, all default values will work
3. Start the project using `docker-compose up` (include ` -d` if you want to run in the background)
4. Wait about 15 seconds, that's how long it will take for laravel to wait before running migrations automatically
5. Access the website on: http://localhost/

## Used external code:
- Laravel default project as demo: https://github.com/laravel/laravel

# Some things to note
- I would usually never use build logic directly in docker-compose, I would always prebuild the image either by hand on in another repo and it's pipeline. Just for this example it fit better.
- Usually I would never include any working credentials in example .env files and directly in docker-compose files.
- Usually I would create a local version of images for services and configure them to run non root user like I did with PHP